# **Convolution**

**Moyenne mobile**, symétrique, commutative, distributive et associative.

<br>

Continu :
$$
(f*g)(x) = \int_{-\infin}^{+\infin} f(x-t) \times g(t) \times dt
$$

Discret :
$$
(f*g)(x) = \sum_{m=-\infin}^{+\infin} f[n-m] \times g(m)
$$

En 2D c'est le même principe.

À partir d'un **noyau** on convolu l'image pour **appliquer** un **filtre**.

<br>

![](images/conv.png)

<br>

Avec ce fonctionnement les **bords** posent problèmes, si on ne fait rien, l'**image** sera **réduite**

![](images/keepborder.png)

On peut donc **étendre** les **bords** (en rajoutant des 0 ou en les recopiant les bords)

![](images/extendborder.png)

Il est également possible d'**adapter** le **traitement**

![](images/adapt.png)




<br><br>


# Filtre passe bas

Lisseur, homogénéise l'image (floute)

## Moyenneur (blur)

**Rapide**, supprime les **hautes fréquences** (détails)

![](images/blur.png)


<br>

## Gaussian

**Lent**, **supprime mieux** les **hautes fréquences** (détails)

![](images/gaussian.png)

<br><br>



# Filtre passe haut

Accentuation (**sharpening**), garde les hautes fréquences dont les **contours** !


![](images/highpass.jpg)

$\boldsymbol{PasseHaut = Original - PasseBas}$

![](images/highpasscompute.png)

$\boldsymbol{Réhausseur = Original + k \times PasseHaut}$ (met en valeur les détails)