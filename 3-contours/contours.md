# **Contours**

Contour <=> frontière

Traduit par une **discontinuité**

On regarde l'**histogramme** d'une **ligne** ou d'une **colonne** pour estimer

![](images/contours.png)

<br>

Contours types

![](images/typescontours1.png)
![](images/typescontours2.png)

Pour la **détection** de **contour** la **somme** des poids du **noyau** de convolution doit être égale à **0**.

<br><br><br>



# **Gradient**

À l'aide de **developpements limités** on approche la **dérivé** de la **courbe** donné par un **histogramme** de ligne ou de colonne.

Le gradient **horizontal** est donc les **dérivés** de toutes les **lignes** et le **vertical** par les **dérivés** de **toutes** les **colonnes**.

$$
f'(x) \approx \frac{f(x+h) - f(x)}{h}
$$

**Gradient** horizontal

$$
\boldsymbol{\nabla{h}[i,j] \approx I[i+1,j] - I[i-1,j]}
$$

C'est une **convolution** avec un masque **[-1,0,1]**

**Attention**, avec un tel filtre la **sensibilité** au **bruit** est très forte, on opte donc **plutôt** sur un **noyau carré**.

![](images/noyaugradhor.png)

**Gradient**

$$
\boldsymbol{||\nabla|| = \sqrt{\nabla{v}^2 \nabla{h}^2}}
$$

Si on cherche les contours en binaire, on prend la norme du gradient.

<br>

Quelques **variantes** du gradient

![](images/variantesgradient.png)

<br>

### Approche de Canny
1. Mesurer amplitude et direction gradient chaque pixel
2. Suppression directionnelle des non maxima locaux
3. Reconstruction des contours par hystérésis (image binaire)

maxima local = le précédant et le suivant ont une norme de gradient inférieur

par hystérésis = si norme du gradient du maxima > seuil, alors le pixel est conservé

<br><br><br>



# **Laplacien**

Toujours à l'aide de **developpements limités** on approche la **dérivé seconde**.

**Laplacien** horizontal

$$
\boldsymbol{\Delta_{h}[i,j] \approx I[i+2,j] - 2I[i+1,j] + I[i,j]}
$$


C'est une **convolution** avec un masque **[1,-2,1]**

**Attention**, avec un tel filtre la **sensibilité** au **bruit** est très forte, on opte donc **plutôt** sur un **noyau carré**.

![](images/laplaciennoyaux.png)


$$
\boldsymbol{\Delta = \Delta_{h} + \Delta_{v}}
$$

![](images/laplaciennoyau.png)

<br>

### Algorithme Marr-Hildreth
1. Calcul du laplacien par convolution
2. Sélection pixels négatifs
3. Pour chacun, calcul du minimum du produit entre le pixel (après laplacien) et l'un des 4 adjacents (avant laplacien) depuis l'image d'origine
4. Si le minimum est inférieur à un seuil (négatif), alors c'est un pixel contour

<br>

### Laplacian of Gaussian (LoG)

Filtre **gaussien avant** le **laplacien**, limite l'impacte du **bruit**.

<br>

### Difference of Gaussian (DoG)

Faire la différence entre 2 gaussiennes approche le laplacien



<br><br><br>



# **Comparaison**

![](images/comparaisonlg.png)

### Avantages Laplacien
- Un seul paramètre (variance de la gaussienne)
- Pas de seuil significatif
- Contours fermés

### Inconvénients Laplacien
- **Sensible** au **bruit**
- **Aucune information** sur l'**orientation**