# **Bruit, c'est quoi ?**

*Détérioration* dû à l'**aquisition**, la **transmission** ou le **codage**

<br>

| Bruit       | Cause                                | Visuel       |
|-------------|--------------------------------------|--------------|
| Gaussian    | électronique, capteur                | normal       |
| Exponential | laser                                | clair        |
| Gamma       | laser                                | clair vénère |
| Impulse     | transmission, capteur CCD, poussière | vénère       |
| Raleigh     | range imaging                        | normal       |
| Uniform     | ?                                    | sombre       |
| Poisson     | courant discontinu                   | ?            |

<br>

En fréquenciel : Bruit <=> hautes fréquences <=> détails

**Estimable** avec des **zones constantes** sur l'**image bruitée**

En **identifiant** le bruit on peut **adapter** le **filtre** à utiliser pour le supprimer.

<br><br><br>



# **Évaluer la restauration**

## **Peak Signal to Noise Ratio (PSNR)**

Mesure la **qualité de reconstruction** à partir de l'image d'origine.

<br>

![](images/formula1.png)

<br>

Variante = **SNR** remplace 255 par la **valeur actuelle du pixel**

**Bon PSNR > 30**

Mais attention, cela ne prend **pas en compte la qualité visuelle** !

<br><br>


## **Structural SIMilarity (SSIM)**

Mesure la **similarité** entre deux images.

**SSIM**(x,y) : symétrique et bornée **[-1,1]**

Dépend **luminance**, **contraste** et **structure**.

<br>

![](images/formula2.png)

On peut aussi calculer le **MSSIM** (moyenne des SSIM avec une fenêtre glissante)

<br><br><br>



# **À quoi ça ressemble ?**

![](images/noises.png)