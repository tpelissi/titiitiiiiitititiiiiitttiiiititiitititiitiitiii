# **Bruit, c'est quoi ?**

*Détérioration* dû à l'**aquisition**, la **transmission** ou le **codage**

<br>

| Bruit       | Cause                                | Visuel       |
|-------------|--------------------------------------|--------------|
| Gaussian    | électronique, capteur                | normal       |
| Exponential | laser                                | clair        |
| Gamma       | laser                                | clair vénère |
| Impulse     | transmission, capteur CCD, poussière | vénère       |
| Raleigh     | range imaging                        | normal       |
| Uniform     | ?                                    | sombre       |
| Poisson     | courant discontinu                   | ?            |

<br>

En fréquenciel : Bruit <=> hautes fréquences <=> détails

**Estimable** avec des **zones constantes** sur l'**image bruitée**

En **identifiant** le bruit on peut **adapter** le **filtre** à utiliser pour le supprimer.

<br><br><br>



# **Évaluer la restauration**

## **Peak Signal to Noise Ratio (PSNR)**

Mesure la **qualité de reconstruction** à partir de l'image d'origine.

<br>

$$
EQM = \frac{1}{mn} \sum_{i=0}^{m-1} \sum_{j=0}^{n-1} (I_{ori}[i,j] - I_{mod}[i,j])^2
$$

$$
PSNR = 10 \times log_{10}(\frac{255^2}{EQM})
$$

<br>

Variante = **SNR** remplace 255 par la **valeur actuelle du pixel**

**Bon PSNR > 30**

Mais attention, cela ne prend **pas en compte la qualité visuelle** !

<br><br>


## **Structural SIMilarity (SSIM)**

Mesure la **similarité** entre deux images.

**SSIM**(x,y) : symétrique et bornée **[-1,1]**

Dépend **luminance**, **contraste** et **structure**.

<br>

$$
l(x,y) = \frac{2\mu_{x}\mu_{y} + K_{l} \times 255}{2\mu_{x}^2 + \mu_{y}^2 + K_{l} \times 255}
$$

$$
c(x,y) = \frac{2\sigma{x}\sigma_{y} + K_{c} \times 255}{2\sigma_{x}^2 + \sigma_{y}^2 + K_{c} \times 255}
$$

$$
s(x,y) = \frac{2\sigma_{xy} + K_{s} \times 255}{2\sigma_{x}^2 \sigma_{y}^2 + K_{s} \times 255}
$$

$$
SSIM(x,y) = l(x,y)^\alpha \times c(x,y)^\beta \times s(x,y)^\gamma
$$

<br>

- $\mu_{x}$ moyenne image x
- $\sigma_{x}$ écart type image x
- $\sigma_{xy} = \frac{1}{N-1} \sum_{i=1}^{N} (x_{i}-\mu_{x})(y_{i}-\mu_{y})$ corrélation entre x et y
- $0 < K << 1$ évite l'explosion si valeur proche de 0 ($K \times 255$)

<br>

En pratique : $\boldsymbol{\alpha = \beta = \gamma = 1}$ et $\boldsymbol{K_{s} = K_{c}/2}$

On peut aussi calculer le **MSSIM** (moyenne des SSIM avec une fenêtre glissante)

<br><br><br>



# **À quoi ça ressemble ?**

![](images/noises.png)