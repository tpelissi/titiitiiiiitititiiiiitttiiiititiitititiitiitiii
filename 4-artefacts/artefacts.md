# **Supprimer les points isolés**

Le problème d'un filtrage linéaire (comme le gaussien), c'est qu'il casse les discontinuités, il homogénise l'image.

Heureusement, il existe des méthodes de filtrage non linéaire qui permettent de conserver les discontinuités dans l'image.

<br><br>



# **Médian**

Lors de la convolution, conserve la **valeur médiane**.

![](images/median.png)

Ceci permet d'**éliminer** les **points** ou **lignes isolés**, cependant, ça mange parfois les contours.

![](images/resultmedian.png)

<br><br>



# **K-Nearest Neighbor (KNN)**

Remplace la valeur **centrale** par la **moyenne** des **k valeurs** les plus **proches** (parmis les voisins).

![](images/knn.png)

A un effet semblabe au filtre médian.

<br><br>



# **Sigma**

Pas clair, page 116 CM1.

Une histoire de seuil et si pas bon alors prend valeur moyenne des pixels du noyau.

<br><br>



# **Min Max**

Prend le **min** et le **max** des pixels **voisins**, si le pixel **central** a une valeur **supérieur**, la remplace par le **max**, et si elle est **inférieur** la remplace par le **min**.

C'est parfait contre le bruit **poivre et sel** !

<br><br>



# **Nagao**

![](images/nagao.png)
(**flemme**, p120 CM1)

<br><br>



# **Bilatéral**

3-10 CM1.2
Filtre passe-bas

http://wcours.gel.ulaval.ca/2017/a/GIF4100/default/5notes/A2017TraitementImagesPartie1PageWeb.pdf

<br>

Ça peut permettre de photoshoper ses photos :E.

![](images/bilateraled.png)

Petit aperçu en faisant varier les paramètres.

![](images/bilateraledddd.png)

<br>

### **Avantages**
- facile à comprendre (*jdois être con du coup :E*)
- facile à adapter
- non itératif

### **Inconvénients**
- non linéaire
- pas de précalcul possible
- implémentation directe lente (mais algo rapide)



<br><br>



# **Non-Local Means (NLM)**

Débruite efficacement en conservant les contours.

2-6 CM1.3


<br>

![](images/comparaisonfiltres.png)
